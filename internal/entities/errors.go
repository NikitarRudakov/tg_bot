package entities

import "errors"

var (
	ErrInvalidParam    = errors.New("invalid parameter")
	ErrIncorrectType   = errors.New("incorrect data type")
	ErrNonExistentURL  = errors.New("non-existent URL")
	ErrSaveFailed      = errors.New("save operation failed")
	ErrGetCourse       = errors.New("failed to get cryptocurrency course")
	ErrStorageInitFail = errors.New("storage initialization failed")
	ErrStorageRead     = errors.New("storage read error")
	ErrUserNotFound    = errors.New("user not found")
	ErrJSONMarshaling  = errors.New("JSON marshaling error")
)
