package ports

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/crocone/tg-bot"
)

func (bot *Tg_bot) handleStartAutoCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Обработка команды handleStartAutoCommand для чата ID: %d", chatID)

	// Проверяем, существует ли чат ID
	if !bot.service.IsChatIDVerified(context.Background(), chatID) {
		// Если чат ID не прошел верификацию, отправляем сообщение о начале с команды "start"
		msg := tgbotapi.NewMessage(chatID, "Пользователь не прошел верификацию. Пожалуйста, начните с команды /start.")
		bot.sendMessage(msg)
		return
	}

	// Отправляем сообщение с запросом ввода от пользователя
	msg := tgbotapi.NewMessage(chatID, "Пожалуйста, введите интервал с какой периодичностью вы хотите получать курсы валют.")
	bot.sendMessage(msg)

	// Создаем контекст с отменой
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // Убеждаемся, что мы отменяем контекст после завершения функции

	var internal string // объявляем переменную для хранения ввода пользователя

	// Ожидаем ввода от пользователя
	updates := bot.bot.GetUpdatesChan(tgbotapi.NewUpdate(0))
	for update := range updates {
		if update.Message != nil && update.Message.Chat.ID == chatID {
			internal = update.Message.Text
			// Проверяем, что введенная строка состоит только из цифр
			if _, err := strconv.Atoi(strings.TrimSpace(internal)); err != nil {
				// Если пользователь ввел не число, отправляем сообщение с просьбой ввести число
				msg := tgbotapi.NewMessage(chatID, "Пожалуйста, введите только число.")
				bot.sendMessage(msg)
			} else {
				// Если введено число, прекращаем цикл и обрабатываем ввод
				break
			}
		}

		// Проверяем, не было ли отмены контекста
		select {
		case <-ctx.Done():
			return // завершаем функцию, если контекст отменен
		default:
		}
	}

	// Если мы дошли до этой точки, значит, ввод пользователя успешен и мы можем продолжить выполнение кода
	// Обработка ввода пользователя
	go func() {
		if err := bot.RunTicker(update, internal); err != nil {
			// Если возникла ошибка при запуске тикера, отправляем сообщение об ошибке
			errMsg := fmt.Sprintf("Ошибка при запуске тикера: %v", err)
			bot.logger.Errorf(errMsg)
			msg := tgbotapi.NewMessage(chatID, errMsg)
			bot.sendMessage(msg)
			return
		}
	}()

	// Отправляем сообщение с курсами пользователю
	msg = tgbotapi.NewMessage(chatID, "Тикер запущен.")
	bot.sendMessage(msg)
}

func (bot *Tg_bot) RunTicker(update tgbotapi.Update, interval string) error {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Запуск тикера для чата ID: %d", chatID)

	// Преобразование интервала в минутах из строки в число
	intervalMinutes, err := strconv.Atoi(interval)
	if err != nil {
		bot.logger.Errorf("Ошибка преобразования интервала: %v", err)
		return err
	}

	// Запуск тикера
	done := make(chan struct{})
	bot.tickers[chatID] = done

	// Обновление статистики и курсов каждые intervalMinutes минут
	ticker := time.NewTicker(time.Duration(intervalMinutes) * time.Minute)
	defer ticker.Stop()

	for {
		select {
		case <-done:
			bot.logger.Infof("Остановка тикера для чата ID: %d", chatID)
			return nil // Остановка тикера
		case <-ticker.C:
			go bot.handleRatesCommand(update)
			go bot.handleStatsCommand(update)
		}
	}

}
func (bot *Tg_bot) handleStopAutoCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Обработка команды handleStopAutoCommand для чата ID: %d", chatID)

	// Остановка тикера для данного чата, если он был запущен
	if ticker, ok := bot.tickers[chatID]; ok {
		ticker <- struct{}{} // Отправляем сигнал для остановки тикера
		delete(bot.tickers, chatID)

		msg := tgbotapi.NewMessage(chatID, "Тикер остановлен.")
		bot.sendMessage(msg)
	} else {
		msg := tgbotapi.NewMessage(chatID, "Тикер не был запущен.")
		bot.sendMessage(msg)
	}
}
