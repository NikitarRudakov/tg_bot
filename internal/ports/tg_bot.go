package ports

import (
	"context"
	"strings"

	tgbotapi "github.com/crocone/tg-bot"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/tg_bot/internal/entities"
)

type Tg_bot struct {
	bot     *tgbotapi.BotAPI
	service Service
	logger  *logrus.Logger
	tickers map[int64]chan struct{}
}

// NewServer конструктор для создания нового экземпляра Server
func NewServer(bot *tgbotapi.BotAPI, service Service, logger *logrus.Logger, tickers map[int64]chan struct{}) (*Tg_bot, error) {
	if service == nil {
		return nil, errors.Wrap(entities.ErrInvalidParam, "new server init failed: service is nil")
	}

	if logger == nil {
		return nil, errors.Wrap(entities.ErrInvalidParam, "logger init failed: logger is nil")
	}

	return &Tg_bot{
		bot:     bot,
		service: service,
		logger:  logger,
		tickers: tickers,
	}, nil
}

var startMenu = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Начать", "start"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Все курсы", "allrates"),
		tgbotapi.NewInlineKeyboardButtonData("Курсы по валютам", "ratesbycurrency"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Все статистика", "allstatistics"),
		tgbotapi.NewInlineKeyboardButtonData("Статистика по валютам", "statisticsbycurrency"),
	),
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("Установить время", "start-auto"),
	),
)

// StartBot начинает прослушивание обновлений и обрабатывает команды
func (bot *Tg_bot) StartBot() error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates := bot.bot.GetUpdatesChan(u)
	if updates == nil {
		return errors.New("failed to get updates channel: updates is nil")
	}
	for update := range updates {
		if update.CallbackQuery != nil {
			bot.callbacks(update)
		} else if update.Message != nil && update.Message.IsCommand() {
			bot.commands(update)
		} else {
			// просто сообщение
		}
	}

	return nil
}

func (bot *Tg_bot) callbacks(update tgbotapi.Update) {
	data := update.CallbackQuery.Data
	chatID := update.CallbackQuery.Message.Chat.ID
	var text string

	switch data {
	case "start":
		text = "Добро пожаловать! Выберите одну из опций ниже."
	case "allrates":
		text = "Пожалуйста, введите список валют, для которых вы хотите увидеть курсы."
	case "ratesbycurrency":
		text = "Пожалуйста, введите список валют, для которых вы хотите увидеть курсы по отдельности."
	case "allstatistics":
		text = "Пожалуйста, введите список валют, для которых вы хотите увидеть статистику."
	case "statisticsbycurrency":
		text = "Пожалуйста, введите список валют, для которых вы хотите увидеть статистику по отдельности."
	case "start-auto":
		text = "Пожалуйста, введите время."
	default:
		text = "Неизвестная команда"
	}

	msg := tgbotapi.NewMessage(chatID, text)
	bot.sendMessage(msg)
}

func (bot *Tg_bot) commands(update tgbotapi.Update) {
	command := update.Message.Command()
	switch command {
	case "start":
		bot.handleStartCommand(update)
	case "rates":
		bot.handleRatesCommand(update)
	case "rates_by_currency":
		bot.handleRatesByTitleCommand(update)
	case "stats":
		bot.handleStatsCommand(update)
	case "stats_by_currency":
		bot.handleStatsByTitleCommand(update)
	case "start_auto":
		bot.handleStartAutoCommand(update)
	case "stop_auto":
		bot.handleStopAutoCommand(update)
	default:
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Неизвестная команда.")
		bot.sendMessage(msg)
	}
}

func extractCoinsFromMessage(text string) []string {
	// Убираем пробелы в начале и в конце строки
	text = strings.TrimSpace(text)
	// Разделяем строку на подстроки по запятой
	coins := strings.Split(text, ",")
	// Убираем пробелы в начале и в конце каждой подстроки и приводим к верхнему регистру
	for i := range coins {
		coins[i] = strings.TrimSpace(strings.ToUpper(coins[i]))
	}
	return coins
}

func (bot *Tg_bot) handleRatesCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Обработка команды handleRatesCommand для чата ID: %d", chatID)

	// Проверка, существует ли чат ID
	if !bot.service.IsChatIDVerified(context.Background(), chatID) {
		// Если чат ID не прошел верификацию, отправляем сообщение о начале с команды "start"
		msg := tgbotapi.NewMessage(chatID, "Пользователь не прошел верификацию. Пожалуйста, начните с команды /start.")
		bot.sendMessage(msg)
		return
	}

	// Если чат ID прошел верификацию, вызываем метод GetAllRates
	ratesMessage, err := bot.service.GetAllRates(context.Background(), chatID)
	if err != nil {
		// Обработка ошибки
		bot.logger.Errorf("Ошибка при получении курсов: %v", err)
		msg := tgbotapi.NewMessage(chatID, "Произошла ошибка при получении курсов.")
		bot.sendMessage(msg)
		return
	}

	// Отправляем сообщение с курсами пользователю
	msg := tgbotapi.NewMessage(chatID, ratesMessage)
	bot.sendMessage(msg)
}

func (bot *Tg_bot) handleRatesByTitleCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Обработка команды handleRatesByTitleCommand для чата ID: %d", chatID)

	// Проверяем, существует ли чат ID
	if !bot.service.IsChatIDVerified(context.Background(), chatID) {
		// Если чат ID не прошел верификацию, отправляем сообщение о начале с команды "start"
		msg := tgbotapi.NewMessage(chatID, "Пользователь не прошел верификацию. Пожалуйста, начните с команды /start.")
		bot.sendMessage(msg)
		return
	}

	// Отправляем сообщение с запросом ввода от пользователя
	msg := tgbotapi.NewMessage(chatID, "Введите сокращенные названия валют через запятую")
	bot.sendMessage(msg)

	// Создаем контекст с отменой
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // Убеждаемся, что мы отменяем контекст после завершения функции

	// Ожидаем ввода от пользователя
	updates := bot.bot.GetUpdatesChan(tgbotapi.NewUpdate(0))
	for update := range updates {
		if update.Message != nil && update.Message.Chat.ID == chatID {
			coins := extractCoinsFromMessage(update.Message.Text)
			if len(coins) == 0 {
				// Если пользователь не ввел ни одной валюты, отправляем сообщение с просьбой ввести валюты
				msg := tgbotapi.NewMessage(chatID, "Пожалуйста, введите список валют для получения курсов.")
				bot.sendMessage(msg)
			} else {
				// Если введены валюты, вызываем метод GetRateByTitles
				ratesMessage, err := bot.service.GetRateByTitles(ctx, coins, chatID)
				if err != nil {
					// Обработка ошибки
					bot.logger.Errorf("Ошибка при получении курсов: %v", err)
					msg := tgbotapi.NewMessage(chatID, "Произошла ошибка при получении курсов.")
					bot.sendMessage(msg)
					return
				}
				// Отправляем сообщение с курсами пользователю
				msg := tgbotapi.NewMessage(chatID, ratesMessage)
				bot.sendMessage(msg)

				// После отправки сообщения завершаем функцию
				return
			}
		}

		// Проверяем, не было ли отмены контекста
		select {
		case <-ctx.Done():
			return // завершаем функцию, если контекст отменен
		default:
		}
	}
}

func (bot *Tg_bot) handleStatsCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Обработка команды handleStatsCommand для чата ID: %d", chatID)

	// Проверка, существует ли чат ID
	if !bot.service.IsChatIDVerified(context.Background(), chatID) {
		// Если чат ID не прошел верификацию, отправляем сообщение о начале с команды "start"
		msg := tgbotapi.NewMessage(chatID, "Пользователь не прошел верификацию. Пожалуйста, начните с команды /start.")
		bot.sendMessage(msg)
		return
	}

	// Если чат ID прошел верификацию, вызываем метод GetAllRates
	ratesMessage, err := bot.service.GetAllStats(context.Background(), chatID)
	if err != nil {
		// Обработка ошибки
		bot.logger.Errorf("Ошибка при получении статистик: %v", err)
		msg := tgbotapi.NewMessage(chatID, "Произошла ошибка при получении статистик.")
		bot.sendMessage(msg)
		return
	}

	// Отправляем сообщение с курсами пользователю
	msg := tgbotapi.NewMessage(chatID, ratesMessage)
	bot.sendMessage(msg)
}

func (bot *Tg_bot) handleStatsByTitleCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID

	bot.logger.Infof("Обработка команды handleRatesByTitleCommand для чата ID: %d", chatID)

	// Проверяем, существует ли чат ID
	if !bot.service.IsChatIDVerified(context.Background(), chatID) {
		// Если чат ID не прошел верификацию, отправляем сообщение о начале с команды "start"
		msg := tgbotapi.NewMessage(chatID, "Пользователь не прошел верификацию. Пожалуйста, начните с команды /start.")
		bot.sendMessage(msg)
		return
	}

	// Отправляем сообщение с запросом ввода от пользователя
	msg := tgbotapi.NewMessage(chatID, "Введите сокращенные названия валют через запятую")
	bot.sendMessage(msg)

	// Создаем контекст с отменой
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // Убеждаемся, что мы отменяем контекст после завершения функции

	// Ожидаем ввода от пользователя
	updates := bot.bot.GetUpdatesChan(tgbotapi.NewUpdate(0))
	for update := range updates {
		if update.Message != nil && update.Message.Chat.ID == chatID {
			coins := extractCoinsFromMessage(update.Message.Text)
			if len(coins) == 0 {
				// Если пользователь не ввел ни одной валюты, отправляем сообщение с просьбой ввести валюты
				msg := tgbotapi.NewMessage(chatID, "Пожалуйста, введите список валют для получения статистик.")
				bot.sendMessage(msg)
			} else {
				// Если введены валюты, вызываем метод GetRateByTitles
				ratesMessage, err := bot.service.GetStatsByTitles(ctx, coins, chatID)
				if err != nil {
					// Обработка ошибки
					bot.logger.Errorf("Ошибка при получении статистик: %v", err)
					msg := tgbotapi.NewMessage(chatID, "Произошла ошибка при получении статистик.")
					bot.sendMessage(msg)
					return
				}
				// Отправляем сообщение с курсами пользователю
				msg := tgbotapi.NewMessage(chatID, ratesMessage)
				bot.sendMessage(msg)

				// После отправки сообщения завершаем функцию
				return
			}
		}

		// Проверяем, не было ли отмены контекста
		select {
		case <-ctx.Done():
			return // завершаем функцию, если контекст отменен
		default:
		}
	}
}

func (bot *Tg_bot) sendMessage(msg tgbotapi.Chattable) {
	if _, err := bot.bot.Send(msg); err != nil {
		bot.logger.Errorf("Ошибка отправки сообщения: %v", err)
	}
}
