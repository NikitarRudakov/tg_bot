package ports

import (
	"context"
	"fmt"

	tgbotapi "github.com/crocone/tg-bot"
)

func (bot *Tg_bot) handleStartCommand(update tgbotapi.Update) {
	chatID := update.Message.Chat.ID
	auth := bot.service.IsChatIDVerified(context.Background(), chatID)
	fmt.Println(auth)

	bot.logger.Infof("Обработка команды handleStartCommand для чата ID: %d", chatID)

	// Проверяем, зарегистрирован ли пользователь
	if auth {
		// Пользователь уже зарегистрирован
		messageText := "Добро пожаловать снова!\n\n" +
			"Доступные команды:\n" +
			"/rates - Получить список всех курсов\n" +
			"/rates_by_currency - Получить курсы по отдельным валютам\n" +
			"/stats - Получить статистику по курсам\n" +
			"/stats_by_currency - Получить статистику по отдельным валютам\n" +
			"/start_auto - Запустить автоматический запрос курсов и статистики\n" +
			"/stop_auto - Остановить автоматический запрос курсов и статистики"

		// Создание сообщения
		msg := tgbotapi.NewMessage(chatID, messageText)
		bot.sendMessage(msg)
	} else {
		// Пользователь не зарегистрирован, регистрируем его
		v, err := bot.service.RegisterUser(context.Background(), chatID)
		if err != nil {
			// Ошибка регистрации
			bot.logger.Errorf("Ошибка при регистрации пользователя: %v", err)
			msg := tgbotapi.NewMessage(chatID, err.Error())
			bot.sendMessage(msg)
		} else {
			// Успешная регистрация
			messageText := v + "\n\n" +
				"Доступные команды:\n" +
				"/rates - Получить список всех курсов\n" +
				"/rates_by_currency - Получить курсы по отдельным валютам\n" +
				"/stats - Получить статистику по курсам\n" +
				"/stats_by_currency - Получить статистику по отдельным валютам\n" +
				"/start_auto - Запустить автоматический запрос курсов и статистики\n" +
				"/stop_auto - Остановить автоматический запрос курсов и статистики"

			// Создание сообщения
			msg := tgbotapi.NewMessage(chatID, messageText)
			bot.sendMessage(msg)
		}
	}
}
