package ports

import "context"

type Service interface {
	GetAllRates(ctx context.Context, chatID int64) (string, error)
	GetRateByTitles(ctx context.Context, coins []string, chatID int64) (string, error)
	GetAllStats(ctx context.Context, chatID int64) (string, error)
	GetStatsByTitles(ctx context.Context, coins []string, chatID int64) (string, error)
	RegisterUser(ctx context.Context, chatID int64) (string, error)
	SaveNewUserCurrencies(ctx context.Context, chatID int64, coins []string, invalidCurrencies []string) error
	IsChatIDVerified(ctx context.Context, chatID int64) bool
}
