package adapters

import (
	"context"
	"encoding/json"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/tg_bot/internal/entities"
)

type PGStorage struct {
	pgx    *pgxpool.Pool
	logger *logrus.Logger
}

func NewPGStorage(pgx *pgxpool.Pool, logger *logrus.Logger) (*PGStorage, error) {
	if pgx == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "storage init failed: %v", pgx)
	}
	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "logger init failed: %v", logger)
	}
	return &PGStorage{
		pgx:    pgx,
		logger: logger,
	}, nil
}

func (p *PGStorage) IsChatIDVerified(ctx context.Context, chatID int64) (bool, error) {
	// Подготовить запрос SQL для проверки существования chatID в базе данных
	q := "SELECT EXISTS (SELECT 1 FROM users_crypto WHERE chat_id = $1)"

	// Выполнить запрос и проверить результат
	var exists bool
	err := p.pgx.QueryRow(ctx, q, chatID).Scan(&exists)
	if err != nil {
		p.logger.Errorf("Ошибка при выполнении запроса: %v", err)
		return false, err // В случае ошибки считаем, что чат не верифицирован
	}

	return exists, nil
}

func (p *PGStorage) SaveUser(ctx context.Context, chatID int64) (string, error) {
	q := `
        INSERT INTO users_crypto (chat_id, currencies)
        VALUES ($1, $2)
        RETURNING id;
    `
	currencies := []string{"BTC", "ETH"}
	currenciesJSON, err := json.Marshal(currencies)
	if err != nil {
		p.logger.Errorf("Failed to marshal currencies: %v", err)
		return "", errors.Wrapf(entities.ErrJSONMarshaling, "Failed to marshal currencies: %v", err)
	}

	var userID int
	err = p.pgx.QueryRow(ctx, q, chatID, currenciesJSON).Scan(&userID)
	if err != nil {
		p.logger.Errorf("Failed to save user: %v", err)
		return "", errors.Wrapf(entities.ErrSaveFailed, "Failed to save user: %v", err)
	}
	p.logger.Info("User saved successfully")

	return "Пользователь успешно зарегистрирован", nil
}

func (p *PGStorage) SaveCryptoUser(ctx context.Context, chatID int64, newCurrencies []string) error {
	// Получаем текущий список криптовалют пользователя
	currentCurrencies, err := p.GetCryptoUser(ctx, chatID)
	if err != nil {
		p.logger.Errorf("Failed to get current currencies for user: %v", err)
		return errors.Wrap(err, "Failed to get current currencies for user")
	}

	// Добавляем новые криптовалюты к текущему списку
	updatedCurrencies := append(currentCurrencies, newCurrencies...)

	// Маршализуем обновленный список криптовалют в формат JSON
	updatedCurrenciesJSON, err := json.Marshal(updatedCurrencies)
	if err != nil {
		p.logger.Errorf("Failed to marshal updated currencies: %v", err)
		return errors.Wrap(err, "Failed to marshal updated currencies")
	}

	// Обновляем запись пользователя с новым списком криптовалют
	q := `
        UPDATE users_crypto
        SET currencies = $1
        WHERE chat_id = $2;
    `
	_, err = p.pgx.Exec(ctx, q, updatedCurrenciesJSON, chatID)
	if err != nil {
		p.logger.Errorf("Failed to update user currencies: %v", err)
		return errors.Wrap(err, "Failed to update user currencies")
	}

	p.logger.Info("User currencies updated successfully")
	return nil
}

func (p *PGStorage) GetCryptoUser(ctx context.Context, chatID int64) ([]string, error) {
	q := `SELECT currencies FROM users_crypto WHERE chat_id = $1;`
	var currenciesJSON []byte
	if err := p.pgx.QueryRow(ctx, q, chatID).Scan(&currenciesJSON); err != nil {
		if err == pgx.ErrNoRows {
			p.logger.Warnf("No user found with chat ID: %d", chatID)
			return nil, errors.Wrapf(entities.ErrUserNotFound, "No user found with chat ID: %d", chatID)
		}
		p.logger.Errorf("Failed to retrieve user: %v", err)
		return nil, errors.Wrapf(entities.ErrStorageRead, "Failed to retrieve user: %v", err)
	}
	var currencies []string
	err := json.Unmarshal(currenciesJSON, &currencies)
	if err != nil {
		p.logger.Errorf("Failed to unmarshal currencies JSON: %v", err)
		return nil, errors.Wrapf(entities.ErrStorageRead, "Failed to unmarshal currencies JSON: %v", err)
	}

	return currencies, nil
}
