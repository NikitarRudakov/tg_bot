package cases

import (
	"context"
)

// UserCrypto сохраняет информацию о пользователях и их криптовалютных операциях.
type UserStorage interface {
	SaveUser(ctx context.Context, chatID int64) (string, error)
	SaveCryptoUser(ctx context.Context, chatID int64, newCurrencies []string) error
	GetCryptoUser(ctx context.Context, chatID int64) ([]string, error)
	IsChatIDVerified(ctx context.Context, chatID int64) (bool, error)
}

type CryptoAPI interface {
	GetCurrency(ctx context.Context, currencies []string) (string, []string, error)
	GetCurrencyStatistics(ctx context.Context, currencies []string) (string, []string, error)
}
