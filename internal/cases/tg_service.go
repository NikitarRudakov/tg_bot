package cases

import (
	"context"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/NikitarRudakov/tg_bot/internal/entities"
)

type CryptoService struct {
	userStorage UserStorage
	cryptoAPI   CryptoAPI
	logger      *logrus.Logger
}

func NewCryptoService(userStorage UserStorage, cryptoAPI CryptoAPI, logger *logrus.Logger) (*CryptoService, error) {
	if userStorage == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "new service: user storage is nil")
	}
	if cryptoAPI == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "new service: crypto API is nil")
	}
	if logger == nil {
		return nil, errors.Wrapf(entities.ErrInvalidParam, "new service: logger is nil")
	}
	return &CryptoService{
		userStorage: userStorage,
		cryptoAPI:   cryptoAPI,
		logger:      logger,
	}, nil
}

func (s *CryptoService) GetAllRates(ctx context.Context, chatID int64) (string, error) {
	// Логируем получение запроса на получение курсов всех криптовалют пользователя
	s.logger.Info("Received request to get all rates")

	// Получаем список криптовалют пользователя
	currencies, err := s.userStorage.GetCryptoUser(ctx, chatID)
	if err != nil {
		s.logger.Errorf("Failed to get user's cryptocurrencies: %v", err)
		return "", errors.Wrap(entities.ErrStorageRead, "failed to get user's cryptocurrencies")
	}

	// Получаем курсы для списка криптовалют
	ratesMessage, _, err := s.cryptoAPI.GetCurrency(ctx, currencies)
	if err != nil {
		s.logger.Errorf("Failed to get currency rates: %v", err)
		return "", errors.Wrap(entities.ErrGetCourse, "failed to get currency rates")
	}

	return ratesMessage, nil
}

func (s *CryptoService) IsChatIDVerified(ctx context.Context, chatID int64) bool {
	// Вызываем метод GetUserCrypto у объекта PGStorage, передавая ему ctx и chatID.
	verified, err := s.userStorage.IsChatIDVerified(ctx, chatID)
	if err != nil {
		// В случае ошибки, логируем ее и возвращаем false.
		s.logger.Errorf("Failed to get user's cryptocurrencies: %v", err)
		return false
	}

	// Возвращаем результат проверки
	return verified
}

func (s *CryptoService) SaveNewUserCurrencies(ctx context.Context, chatID int64, coins []string, invalidCurrencies []string) error {
	// Получаем криптовалюты пользователя
	userCurrencies, err := s.userStorage.GetCryptoUser(ctx, chatID)
	if err != nil {
		s.logger.Errorf("Failed to get user's cryptocurrencies: %v", err)
		return errors.Wrap(entities.ErrStorageRead, "failed to get user's cryptocurrencies")
	}
	var newCurrencies []string
	for _, coin := range coins {
		exists := false
		for _, currency := range invalidCurrencies {
			if coin == currency {
				exists = true
				break
			}
		}
		if !exists {
			newCurrencies = append(newCurrencies, coin)
		}
	}
	var validCurrencies []string
	for _, currency := range newCurrencies {
		exists := false
		for _, userCurrency := range userCurrencies {
			if currency == userCurrency {
				exists = true
				break
			}
		}
		if !exists {
			validCurrencies = append(validCurrencies, currency)
		}
	}
	// Сохраняем новые криптовалюты пользователя
	if len(validCurrencies) > 0 {
		err := s.userStorage.SaveCryptoUser(ctx, chatID, validCurrencies)
		if err != nil {
			s.logger.Errorf("Failed to save new user currencies: %v", err)
			return errors.Wrapf(entities.ErrSaveFailed, "failed to save new user currencies: %v", err)
		}
	}

	return nil
}

func (s *CryptoService) GetRateByTitles(ctx context.Context, coins []string, chatID int64) (string, error) {
	// Логируем получение запроса на получение курсов по названиям криптовалют
	s.logger.Info("Received request to get rates by titles")

	// Получаем криптовалюты из внешнего источника по переданным монетам
	ratesMessage, invalidCurrencies, err := s.cryptoAPI.GetCurrency(ctx, coins)
	if err != nil {
		s.logger.Errorf("Failed to get currency rates: %v", err)
		return "", err
	}
	err = s.SaveNewUserCurrencies(ctx, chatID, coins, invalidCurrencies)
	if err != nil {
		s.logger.Errorf("Failed to save new user currencies: %v", err)
		return "", err
	}

	return ratesMessage, nil
}

func (s *CryptoService) GetAllStats(ctx context.Context, chatID int64) (string, error) {
	// Логируем получение запроса на получение статистики всех криптовалют пользователя
	s.logger.Info("Received request to get all stats")

	// Получаем список криптовалют пользователя
	currencies, err := s.userStorage.GetCryptoUser(ctx, chatID)
	if err != nil {
		s.logger.Errorf("Failed to get user's cryptocurrencies: %v", err)
		return "", errors.Wrap(entities.ErrStorageRead, "failed to get user's cryptocurrencies")
	}

	// Получаем статистику для списка криптовалют
	statsMessage, _, err := s.cryptoAPI.GetCurrencyStatistics(ctx, currencies)
	if err != nil {
		s.logger.Errorf("Failed to get currency statistics: %v", err)
		return "", errors.Wrap(entities.ErrGetCourse, "failed to get currency statistics")
	}

	return statsMessage, nil
}

func (s *CryptoService) GetStatsByTitles(ctx context.Context, coins []string, chatID int64) (string, error) {
	// Логируем получение запроса на получение статистики по названиям криптовалют
	s.logger.Info("Received request to get stats by titles")

	// Получаем криптовалюты из внешнего источника по переданным монетам
	statsMessage, invalidCurrencies, err := s.cryptoAPI.GetCurrencyStatistics(ctx, coins)
	if err != nil {
		s.logger.Errorf("Failed to get currency statistics: %v", err)
		return "", err
	}
	err = s.SaveNewUserCurrencies(ctx, chatID, coins, invalidCurrencies)
	if err != nil {
		s.logger.Errorf("Failed to save new user currencies: %v", err)
		return "", err
	}

	return statsMessage, nil
}

func (s *CryptoService) RegisterUser(ctx context.Context, chatID int64) (string, error) {
	// Логируем получение запроса на регистрацию пользователя
	s.logger.Info("Received request to register user")

	// Вызываем метод сохранения пользователя
	message, err := s.userStorage.SaveUser(ctx, chatID)
	if err != nil {
		s.logger.Errorf("Failed to save user: %v", err)
		return "", err
	}

	// Возвращаем сообщение об успешной регистрации
	return message, nil
}
