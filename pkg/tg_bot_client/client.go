package tgbotclient

import (
	"context"
	"fmt"

	tgbotapi "github.com/crocone/tg-bot"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/configs"
)

// NewBot создает новый экземпляр Telegram бота с настройками из .env файла
func NewBot(ctx context.Context, config *configs.Config) (*tgbotapi.BotAPI, error) {
	var bot *tgbotapi.BotAPI
	bot, err := tgbotapi.NewBotAPI(config.Token)
	if err != nil {
		return nil, fmt.Errorf("Не удалось инициализировать Telegram бота: %v", err)
	}

	return bot, nil
}
