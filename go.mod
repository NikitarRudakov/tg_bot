module gitlab.com/NikitarRudakov/tg_bot

go 1.21.0

require (
	github.com/crocone/tg-bot v0.0.0-20230406135341-8d3e6a822eaf
	github.com/jackc/pgx/v4 v4.18.1
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.9.3
	gitlab.com/NikitarRudakov/cryptocurrency_converter v0.0.0-20240216060756-375ff30502f1
)

require (
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.14.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.2 // indirect
	github.com/jackc/pgservicefile v0.0.0-20231201235250-de7065d80cb9 // indirect
	github.com/jackc/pgtype v1.14.1 // indirect
	github.com/jackc/puddle v1.3.0 // indirect
	github.com/subosito/gotenv v1.6.0 // indirect
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
