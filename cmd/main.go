package main

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/configs"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/db/postgresql"
	"gitlab.com/NikitarRudakov/cryptocurrency_converter/pkg/logging"
	"gitlab.com/NikitarRudakov/tg_bot/internal/adapters"
	"gitlab.com/NikitarRudakov/tg_bot/internal/cases"
	"gitlab.com/NikitarRudakov/tg_bot/internal/ports"
	tgbotclient "gitlab.com/NikitarRudakov/tg_bot/pkg/tg_bot_client"
)

func main() {
	logger := logging.GetLogger()
	cfg, err := configs.GetConfig()
	if err != nil {
		logger.Fatalf("Error getting config: %v", err)
	}

	bot, err := tgbotclient.NewBot(context.TODO(), cfg)
	fmt.Println(cfg.Token)
	if err != nil {
		logger.Fatalf("Error creating Telegram bot: %v", err)
	}

	postgreSQLClient, err := postgresql.NewClient(context.TODO(), 3, cfg)
	if err != nil {
		logger.Fatalf("Error creating PostgreSQL client: %v", err)
	}

	pgStorage, err := adapters.NewPGStorage(postgreSQLClient, logger)
	if err != nil {
		logger.Fatalf("Error creating PGStorage: %v", err)
	}

	client := &http.Client{}
	scrapperService, err := adapters.NewCurrencyCryptoClient(logger, client)
	if err != nil {
		logger.Fatalf("Error creating ScrapperService: %v", err)
	}

	cryptoService, err := cases.NewCryptoService(pgStorage, scrapperService, logger)
	if err != nil {
		logger.Fatalf("Error creating CryptoService: %v", err)
	}
	tickers := make(map[int64]chan struct{})
	tgBot, err := ports.NewServer(bot, cryptoService, logger, tickers)
	if err != nil {
		logger.Fatalf("Error creating Server: %v", err)
	}

	err = tgBot.StartBot()
	if err != nil {
		logger.Fatalf("Error starting Telegram bot: %v", err)
	}
}
